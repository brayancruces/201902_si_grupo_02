-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Cliente` (
  `idCliente` INT NOT NULL,
  `nCliente` VARCHAR(45) NULL,
  `aCliente` VARCHAR(45) NULL,
  PRIMARY KEY (`idCliente`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Promocion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Promocion` (
  `idPromocion` INT NOT NULL,
  `nPromocion` VARCHAR(45) NULL,
  `fINICIO` DATETIME NULL,
  `fFin` DATETIME NULL,
  PRIMARY KEY (`idPromocion`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Reserva`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Reserva` (
  `id` INT NOT NULL,
  `fecha` DATETIME NULL,
  `Reservacol` VARCHAR(45) NULL,
  `Cliente_idCliente` INT NOT NULL,
  `Promocion_idPromocion` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Reserva_Cliente1_idx` (`Cliente_idCliente` ASC) VISIBLE,
  INDEX `fk_Reserva_Promocion1_idx` (`Promocion_idPromocion` ASC) VISIBLE,
  CONSTRAINT `fk_Reserva_Cliente1`
    FOREIGN KEY (`Cliente_idCliente`)
    REFERENCES `mydb`.`Cliente` (`idCliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Reserva_Promocion1`
    FOREIGN KEY (`Promocion_idPromocion`)
    REFERENCES `mydb`.`Promocion` (`idPromocion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Especie`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Especie` (
  `idEspecie` INT NOT NULL,
  `nEspecie` VARCHAR(45) NULL,
  PRIMARY KEY (`idEspecie`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Raza`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Raza` (
  `idRaza` INT NOT NULL,
  `nRaza` VARCHAR(45) NULL,
  `Especie_idEspecie` INT NOT NULL,
  PRIMARY KEY (`idRaza`),
  INDEX `fk_Raza_Especie1_idx` (`Especie_idEspecie` ASC) VISIBLE,
  CONSTRAINT `fk_Raza_Especie1`
    FOREIGN KEY (`Especie_idEspecie`)
    REFERENCES `mydb`.`Especie` (`idEspecie`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Mascota`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Mascota` (
  `idMascota` INT NOT NULL,
  `nMascota` VARCHAR(45) NULL,
  `idCliente` INT NULL,
  `Mascotacol` VARCHAR(45) NULL,
  `Raza_idRaza` INT NOT NULL,
  PRIMARY KEY (`idMascota`),
  INDEX `fk_Mascota_Raza1_idx` (`Raza_idRaza` ASC) VISIBLE,
  CONSTRAINT `fk_Mascota_Raza1`
    FOREIGN KEY (`Raza_idRaza`)
    REFERENCES `mydb`.`Raza` (`idRaza`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Programacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Programacion` (
  `ReservaID` INT NOT NULL,
  `Programacioncol` VARCHAR(45) NULL,
  `Reserva_id` INT NOT NULL,
  `Mascota_idMascota` INT NOT NULL,
  PRIMARY KEY (`ReservaID`),
  INDEX `fk_Programacion_Reserva1_idx` (`Reserva_id` ASC) VISIBLE,
  INDEX `fk_Programacion_Mascota1_idx` (`Mascota_idMascota` ASC) VISIBLE,
  CONSTRAINT `fk_Programacion_Reserva1`
    FOREIGN KEY (`Reserva_id`)
    REFERENCES `mydb`.`Reserva` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Programacion_Mascota1`
    FOREIGN KEY (`Mascota_idMascota`)
    REFERENCES `mydb`.`Mascota` (`idMascota`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Servicio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Servicio` (
  `id` INT NOT NULL,
  `nServicio` VARCHAR(45) NULL,
  `cServicio` DECIMAL(8,2) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Programacion_Servicio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Programacion_Servicio` (
  `id` INT NOT NULL,
  `Cantidad` VARCHAR(45) NULL,
  `Monto` DECIMAL(8,2) NULL,
  `Servicio_id` INT NOT NULL,
  `Programacion_ReservaID` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Programacion_Servicio_Servicio1_idx` (`Servicio_id` ASC) VISIBLE,
  INDEX `fk_Programacion_Servicio_Programacion1_idx` (`Programacion_ReservaID` ASC) VISIBLE,
  CONSTRAINT `fk_Programacion_Servicio_Servicio1`
    FOREIGN KEY (`Servicio_id`)
    REFERENCES `mydb`.`Servicio` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Programacion_Servicio_Programacion1`
    FOREIGN KEY (`Programacion_ReservaID`)
    REFERENCES `mydb`.`Programacion` (`ReservaID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Veterinaria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Veterinaria` (
  `idVeterinaria` INT NOT NULL,
  `nVeterinaria` VARCHAR(45) NULL,
  PRIMARY KEY (`idVeterinaria`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Sede`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Sede` (
  `id` INT NOT NULL,
  `nSede` VARCHAR(45) NOT NULL,
  `nDireccion` VARCHAR(45) NULL,
  `Veterinaria_idVeterinaria` INT NOT NULL,
  `latitud` VARCHAR(45) NULL,
  `longitud` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Sede_Veterinaria1_idx` (`Veterinaria_idVeterinaria` ASC) VISIBLE,
  CONSTRAINT `fk_Sede_Veterinaria1`
    FOREIGN KEY (`Veterinaria_idVeterinaria`)
    REFERENCES `mydb`.`Veterinaria` (`idVeterinaria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Reserva_Pago`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Reserva_Pago` (
  `id` INT NOT NULL,
  `fecha` DATETIME NULL,
  `Monto` DECIMAL(8,2) NULL,
  `Reserva_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Reserva_Pago_Reserva1_idx` (`Reserva_id` ASC) VISIBLE,
  CONSTRAINT `fk_Reserva_Pago_Reserva1`
    FOREIGN KEY (`Reserva_id`)
    REFERENCES `mydb`.`Reserva` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Enfermedad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Enfermedad` (
  `idEnfermedad` INT NOT NULL,
  `nEnfermedad` VARCHAR(45) NULL,
  PRIMARY KEY (`idEnfermedad`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Personal_Veteriano`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Personal_Veteriano` (
  `idVeterinario` INT NOT NULL,
  `nVeterinario` VARCHAR(45) NULL,
  `Sede_id` INT NOT NULL,
  PRIMARY KEY (`idVeterinario`),
  INDEX `fk_Veterinario_Sede_idx` (`Sede_id` ASC) VISIBLE,
  CONSTRAINT `fk_Veterinario_Sede`
    FOREIGN KEY (`Sede_id`)
    REFERENCES `mydb`.`Sede` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Diagnostico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Diagnostico` (
  `idDiagnostico` INT NOT NULL,
  `fecha` DATETIME NULL,
  `Comentario` TEXT NULL,
  `Enfermedad_idEnfermedad` INT NOT NULL,
  `Mascota_idMascota` INT NOT NULL,
  `Veterinario_idVeterinario` INT NOT NULL,
  PRIMARY KEY (`idDiagnostico`),
  INDEX `fk_Diagnostico_Enfermedad1_idx` (`Enfermedad_idEnfermedad` ASC) VISIBLE,
  INDEX `fk_Diagnostico_Mascota1_idx` (`Mascota_idMascota` ASC) VISIBLE,
  INDEX `fk_Diagnostico_Veterinario1_idx` (`Veterinario_idVeterinario` ASC) VISIBLE,
  CONSTRAINT `fk_Diagnostico_Enfermedad1`
    FOREIGN KEY (`Enfermedad_idEnfermedad`)
    REFERENCES `mydb`.`Enfermedad` (`idEnfermedad`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Diagnostico_Mascota1`
    FOREIGN KEY (`Mascota_idMascota`)
    REFERENCES `mydb`.`Mascota` (`idMascota`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Diagnostico_Veterinario1`
    FOREIGN KEY (`Veterinario_idVeterinario`)
    REFERENCES `mydb`.`Personal_Veteriano` (`idVeterinario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Servicio_Sede`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Servicio_Sede` (
  `Servicio_id` INT NOT NULL,
  `Sede_id` INT NOT NULL,
  INDEX `fk_Servicio_Sede_Servicio1_idx` (`Servicio_id` ASC) VISIBLE,
  INDEX `fk_Servicio_Sede_Sede1_idx` (`Sede_id` ASC) VISIBLE,
  CONSTRAINT `fk_Servicio_Sede_Servicio1`
    FOREIGN KEY (`Servicio_id`)
    REFERENCES `mydb`.`Servicio` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Servicio_Sede_Sede1`
    FOREIGN KEY (`Sede_id`)
    REFERENCES `mydb`.`Sede` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Promocion_Servicio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Promocion_Servicio` (
  `Cantidad` INT NOT NULL,
  `Costo` DECIMAL(8,2) NULL,
  `Servicio_id` INT NOT NULL,
  `Promocion_idPromocion` INT NOT NULL,
  PRIMARY KEY (`Servicio_id`),
  INDEX `fk_Promocion_Servicio_Promocion1_idx` (`Promocion_idPromocion` ASC) VISIBLE,
  CONSTRAINT `fk_Promocion_Servicio_Servicio1`
    FOREIGN KEY (`Servicio_id`)
    REFERENCES `mydb`.`Servicio` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Promocion_Servicio_Promocion1`
    FOREIGN KEY (`Promocion_idPromocion`)
    REFERENCES `mydb`.`Promocion` (`idPromocion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Usuario` (
  `id` INT NOT NULL,
  `usuario` VARCHAR(45) NULL,
  `clave` VARCHAR(45) NULL,
  `Cliente_idCliente` INT NULL,
  `Personal_Veteriano_idVeterinario` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Usuario_Cliente1_idx` (`Cliente_idCliente` ASC) VISIBLE,
  INDEX `fk_Usuario_Personal_Veteriano1_idx` (`Personal_Veteriano_idVeterinario` ASC) VISIBLE,
  CONSTRAINT `fk_Usuario_Cliente1`
    FOREIGN KEY (`Cliente_idCliente`)
    REFERENCES `mydb`.`Cliente` (`idCliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Usuario_Personal_Veteriano1`
    FOREIGN KEY (`Personal_Veteriano_idVeterinario`)
    REFERENCES `mydb`.`Personal_Veteriano` (`idVeterinario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
